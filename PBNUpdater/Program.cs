﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading;

namespace PBNUpdater
{
    class Program
    {
        static void Main(string[] args)
        {
            string specialFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var domainListPath = File.ReadAllLines(specialFolder + @"\domainsToUpdate.txt");
            var domainLoginsPath = File.ReadAllLines(specialFolder + @"\domainsLogins.txt");
            var domainPasswordsPath = File.ReadAllLines(specialFolder + @"\domainsPasswords.txt");
            List<string> domainsList = new List<string>(domainListPath);
            List<string> domainsLogins = new List<string>(domainLoginsPath);
            List<string> domainsPasswords = new List<string>(domainPasswordsPath);
            string login;
            string password;
            string domainUrl = "";

            IWebDriver driver;

            // Setup browser
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.AddArguments("--lang=en", "--disable-infobars", "--disable-gpu");
            chromeOptions.AddUserProfilePreference("credentials_enable_service", false);
            chromeOptions.AddUserProfilePreference("profile.password_manager_enabled", false);

            // Start browser
            driver = new ChromeDriver(chromeOptions);
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Window.Size = new Size(1800, 1000);

            foreach (string domain in domainsList)
            {
                login = domainsLogins[domainsList.IndexOf(domain)];
                password = domainsPasswords[domainsList.IndexOf(domain)];

                try
                {
                    Wordpress.Login(driver, domain, ref domainUrl, login, password);
                    Wordpress.UpdateWordpress(driver, domain, domainUrl);
                    Wordpress.UpdatePlugins(driver, domain, domainUrl);

                    Thread.Sleep(1000);
                }
                catch (Exception)
                {
                }
            }

            driver.Close();
            Environment.Exit(0);
        }
    }
}