﻿using OpenQA.Selenium;
using System;
using System.Threading;

namespace PBNUpdater
{
    public static class Wordpress
    {
        public static void Login(this IWebDriver driver, string domain, ref string domainUrl, string login, string password)
        {
            bool wordpressLoginDisplayed = true;
            do
            {

                try
                {
                    driver.Url = "http://" + domain;
                    domainUrl = driver.Url;
                    domainUrl = domainUrl.Substring(0, domainUrl.Length - 1);

                    driver.Url = domainUrl + "/wp-admin/";
                    Thread.Sleep(3000);

                    IWebElement wordpressLoginField = driver.FindElement(By.XPath("//*[@id='user_login']"));
                    if (wordpressLoginField.Displayed == true)
                    {
                        wordpressLoginField.Clear();
                        wordpressLoginField.SendKeys(login);
                        Thread.Sleep(2000);

                        IWebElement wordpressPasswordField = driver.FindElement(By.XPath("//*[@id='user_pass']"));
                        wordpressPasswordField.Clear();
                        wordpressPasswordField.SendKeys(password);
                        Thread.Sleep(2000);

                        IWebElement wordpressLoginButton = driver.FindElement(By.XPath("//*[@id='wp-submit']"));
                        wordpressLoginButton.Click();
                        Thread.Sleep(3000);

                        IWebElement adminMenu = driver.FindElement(By.XPath("//*[@id='adminmenumain']"));
                        if (adminMenu.Displayed == true)
                        {
                            wordpressLoginDisplayed = false;
                        }
                    }
                    else
                    {
                        wordpressLoginDisplayed = false;
                    }
                }
                catch (Exception)
                {
                    driver.Url = domainUrl + "/wp-admin/";
                    Thread.Sleep(2000);

                    if (driver.FindElement(By.XPath("//*[@id=\"welcome-panel\"]")).Displayed == true)
                    {
                        wordpressLoginDisplayed = false;
                    }
                }
            } while (wordpressLoginDisplayed == true);
        }

        public static void UpdateWordpress(this IWebDriver driver, string domain, string domainUrl)
        {
            try
            {
                driver.Url = domainUrl + "/wp-admin/update-core.php";
                Thread.Sleep(2000);

                IWebElement upgradeButton = driver.FindElement(By.XPath("//input[@id=\"upgrade\" and @class=\"button button-primary regular\"]"));
                upgradeButton.Click();
                Thread.Sleep(2000);
            }
            catch (Exception)
            {
                try
                {
                    driver.Url = domainUrl + "/wp-admin/update-core.php";
                    Thread.Sleep(2000);

                    IWebElement upgradeButton = driver.FindElement(By.XPath("//input[@id=\"upgrade\" and @class=\"button button-primary regular\"]"));
                    upgradeButton.Click();
                    Thread.Sleep(2000);
                }
                catch (Exception)
                {
                }
            }
            Thread.Sleep(15000);
        }

        public static void UpdatePlugins(this IWebDriver driver, string domain, string domainUrl)
        {
            try
            {
                driver.Url = domainUrl + "/wp-admin/update-core.php";
                Thread.Sleep(2000);

                IWebElement checkAllButton = driver.FindElement(By.XPath("//*[@id=\"plugins-select-all\"]"));
                checkAllButton.Click();
                Thread.Sleep(2000);

                IWebElement updateButton = driver.FindElement(By.XPath("//*[@id=\"upgrade-plugins-2\" or @id='upgrade-plugins']"));
                updateButton.Click();
                Thread.Sleep(2000);
            }
            catch (Exception)
            {
                try
                {
                    driver.Url = domainUrl + "/wp-admin/update-core.php";
                    Thread.Sleep(2000);

                    IWebElement checkAllButton = driver.FindElement(By.XPath("//*[@id=\"plugins-select-all\"]"));
                    checkAllButton.Click();
                    Thread.Sleep(2000);

                    IWebElement updateButton = driver.FindElement(By.XPath("//*[@id=\"upgrade-plugins-2\" or @id='upgrade-plugins']"));
                    updateButton.Click();
                    Thread.Sleep(2000);
                }
                catch (Exception)
                {
                }
            }
            Thread.Sleep(120000);
        }
    }
}